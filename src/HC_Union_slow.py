import numpy as np
import pickle
import argparse
import pathlib

# S corresponds to the used dataset, saved as a numpy array
# k corresponds to the number of clusters

# C corresponds to the set of clustercenters
# cluster_sets = Clustering

def k_means_plusplus(S, k):

    Z = []
    # Sampling an element with a uniform distribution
    z1 = S[np.random.choice(np.arange(S.shape[0]))]
    Z.append(z1)

    # Initializing distance arrays
    d = np.zeros(S.shape[0])
    for i in range(S.shape[0]):
        d[i] = np.linalg.norm(S[i] - z1, ord = 2)**2

    # Initializing probabilities
    probs = np.zeros(S.shape[0])
    for i in range(1, k):
        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)

        # Sampling next cluster center
        z_new = S[np.random.choice(np.arange(S.shape[0]), p = probs)]
        Z.append(z_new)
        # Calculating minimum distance
        for j in range(S.shape[0]):
            d[j] = np.minimum(d[j], np.linalg.norm(S[j] - z_new, ord=2)**2)
    return np.array(Z)


# Algorithm to improve k_means_plus_plus approximation to O(1)
def local_search_plusplus(S, C, epsilon = 1):
    # If every point is already used as a cluster_center local_search_plusplus is no longer needed
    if C.shape[0] == S.shape[0]:
        return C

    # Initialize probability distribution and distance arrays
    probs = np.zeros(S.shape[0])
    d = np.zeros(S.shape[0])

    for k in range(C.shape[0] * epsilon):
        for l in range(S.shape[0]):
            d[l] = np.min([np.linalg.norm(S[l] - c, ord=2) ** 2 for c in C])

        for l in range(S.shape[0]):
            probs[l] = d[l]/np.sum(d)

        # Sampling new cluster center
        z_new = S[np.random.choice(np.arange(S.shape[0]), p=probs)]

        # Swap Itarations for the argmin calculations
        _ , cost = calculate_k_means_cluster_cost(S, C)
        cost_list = []
        final_cost = cost
        final_cluster = C
        for i in range(C.shape[0]):
            cluster_copy = C.copy()
            cluster_copy[i] = z_new
            _ , copy_cost = calculate_k_means_cluster_cost(S, cluster_copy)
            if copy_cost < final_cost:
                final_cost = copy_cost
                final_cluster = cluster_copy

        C = final_cluster
    return C


def k_means(S, k, C = None):

    if C is None:
        C = np.random.random((k, S.shape[1]))

    # Error handling
    else:
        assert C.shape[0] == k, "less/more Cluster centers in C than given k"

    # Addition of 1 is needed for while loop to run at least one time
    C_converge = C.copy() + 1

    # Potential floating point error is being considered
    while np.abs(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]) > 0.0001:

        C_converge = C.copy()

        cluster_sets = {}
        for i in range(k):
            cluster_sets[i] = []

        # Computing minimal distance cluster center for each data point
        for x in S:
            cluster_idx = np.argmin([np.linalg.norm(x - c, ord = 2)**2 for c in C])
            cluster_sets[cluster_idx].append(x)

        buffer = 0
        for i in range(k):

            # Error management, in case no data points are in the vicinity of a given cluster center
            if len(cluster_sets[i]) == 0:
                buffer += 1

            else:
                # (i - buffer) is used to facilitate computation of C array with less Clusters
                C[i - buffer] = np.sum(np.array(cluster_sets[i]), axis = 0)/len(cluster_sets[i])

        # If lloyds algorithm algorithm computed less than k Clusters
        if buffer > 0:
            print("algorithm computed less than k clusters, case is being handled")
            k -= buffer
            copy = C.copy()
            C = np.zeros((k, copy.shape[1]))
            for i in range(k):
                C[i] = copy[i]

        if buffer == 0:
            print("following is converge_cost : " + str(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]))

    return cluster_sets, C


# Calculates cluster costs if cluster center is understood as the arithmetic median of a given cluster
def calculate_opt_k_means_cost(cluster_set):
    cost = 0
    for i in range(len(cluster_set)):
        median = np.sum(np.array(cluster_set[i]), axis = 0)/len(cluster_set[i])

        new_cost = np.sum([np.linalg.norm(x - median, ord = 2)**2 for x in cluster_set[i]])

        cost+=new_cost

    return cost


# Calculates clusters and cluster costs if a set of cluster_centers is given
def calculate_k_means_cluster_cost(data, cluster_centers):
    cluster_sets = {}
    costs = 0
    for i in range(cluster_centers.shape[0]):
        cluster_sets[i] = []
    for x in data:
        cluster_idx = np.argmin([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        costs += np.min([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        cluster_sets[cluster_idx].append(x)

    return cluster_sets, costs


# Finishes Clustering computed by Union - HC
def finish_clustering(data, h_clustering):
    if len(h_clustering) == len(data):
        return h_clustering


    i = 0
    while len(h_clustering[i]) > 1:
        new_clusters = {}
        merge_list = [[] for element in h_clustering[i + 1]]

        # Check if you have >=2 more clusters in next list, than in current one, if you only have one,
        # Clustering does not need to be augmented

        if len(h_clustering[i]) - len(h_clustering[i + 1]) > 1:

            # Create merge_list
            boolean = True
            for l in range(len(h_clustering[i])):
                for m in range(len(h_clustering[i + 1])):
                    # Check subset relation
                    for element in h_clustering[i][l]:
                        if not any([np.array_equal(element, x) for x in h_clustering[i + 1][m]]):
                            boolean = False

                    if boolean:
                        merge_list[m].append(l)
                    boolean = True

            # Merge_list created

            # Merging two cluster
            for o in range(len(merge_list)):
                if len(merge_list[o]) > 1:
                    new_clusters[0] = h_clustering[i][merge_list[o][0]] + h_clustering[i][merge_list[o][1]]
                    break

            # Adding remaining clusters to clustering
            buffer = 0
            for p in range(len(h_clustering[i])):
                if p == merge_list[o][0] or p == merge_list[o][1]:
                    buffer += 1

                else:
                    # Considering the first slot is already used by our merge step, an addition of 1 to our index is imperative
                    new_clusters[p - buffer + 1] = h_clustering[i][p]

            h_clustering.insert(i + 1, new_clusters)

        i += 1

    return h_clustering


def test_hierarchical(final_clustering):
    true_final_clustering = final_clustering
    for i in range(len(true_final_clustering) - 1):
        cluster_dict = true_final_clustering[i]
        cluster_dict2 = true_final_clustering[i+1]
        # element_list corresponds to a cluster
        # Now one wants to check if element_list is a subset of another cluster in the next clustering
        for _, element_list in cluster_dict.items():
            # Boolean, which specifies if a given cluster is a subset of a cluster in a higher layer
            are_elements_in_a_element_list_2 = False
            for _, element_list2 in cluster_dict2.items():
                # element stands for a element in the cluster
                for element in element_list:
                    # If no element in the cluster2 is equal to element
                    if not any([np.array_equal(element, x) for x in element_list2]):
                        boolean = False
                    else:
                        boolean = True
                if boolean:
                    are_elements_in_a_element_list_2 = True
            assert are_elements_in_a_element_list_2, "CLUSTERING ERROR" + str(i)

    return True

def augment(S, cluster_sets, k, iteration, cluster_centers_list, gamma = 2, use_lloyds = False):

    # Lists for final output
    final = []
    final_costs = []

    for p in range(1, k):
        # cluster_centers = k_means_plusplus(S, k - p)
        cluster_centers = cluster_centers_list[k - p - 1]

        if use_lloyds:
            _, cluster_centers = k_means(S, C = cluster_centers)

        used_cluster_centers = []
        for i in range(k):
            # Calculate cluster center with lowest cost to cluster
            used_cluster_centers.append((np.argmin([np.sum([np.linalg.norm(element - c, ord = 2)**2 for element in cluster_sets[i]]) for c in cluster_centers])))

        # Saving cluster centers, which were 'used' more than once, to facilitate merging step of clusters
        occurrences = [x for x in used_cluster_centers if used_cluster_centers.count(x) > 1]
        # k_new =  new number of Clusters
        k_new = len(occurrences) - 1
        # Deleting duplicates from occurrences for merge step
        occurrences_no_duplicates = list(set(occurrences))

        # Merge step: examining double/multiple occurrences of cluster centers
        new_cluster_sets = {}
        for m, element in enumerate(occurrences_no_duplicates):
            l1 = [l for l, k2 in enumerate(used_cluster_centers) if (k2 == element)]

            new = []
            for l1_element in l1:
                new = new + cluster_sets[l1_element]
            new_cluster_sets[m] = new
            l1 = []

        # Examining single occurrences of cluster centers
        # Used_cluster_centers was constructed in order -> i'th index element stands for cluster chosen for i'th cluster
        for element in used_cluster_centers:
            if element not in occurrences_no_duplicates:
                m += 1
                new_cluster_sets[m] = cluster_sets[used_cluster_centers.index(element)]

        # new_cluster is finished
        costs = calculate_opt_k_means_cost(new_cluster_sets)
        if costs < (2 * gamma)**(iteration + 1):
            final.append(new_cluster_sets)
            final_costs.append(costs)
        else:
            final.append(new_cluster_sets)
            final_costs.append(float('inf'))


    print("final_costs" + str(final_costs))
    print((2 * gamma)**(iteration + 1))
    idx = np.argmin(final_costs)

    if final_costs[idx] == float('inf'):
        print("all costs +inf")
        return None

    else:
        l2 = []
        for i in range(len(final_costs)):
            if final_costs[i] < float('inf'):
                l2.append(i)

        var = np.max(l2)

        return final[var]


# Complete algorithm using all of the defined functions
def algorithm_old(data):
    cs = {}
    # cluster_sets in first iteration
    for i in range(len(data)):
        cs[i] = [data[i]]

    hierarchical_clustering = []
    iteration = 0

    # Generating k_means_plus_plus solution, for comparison later
    cluster_centers_list = []
    for i in range(1, len(data) + 1):
        print("generating clusters step:" + str(i))
        x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)))[1]

        # There is a small probability that k_means returns less than i centers, the following case is being handled here
        for j in range(3):
            if x.shape[0] < i:
                x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)))[1]
            if x.shape[0] == 1:
                break
        # If after 3 iterations we do not have a new solution, we generate a solution without lloyds algorithm, guaranteeing correct cluster size
        if x.shape[0] < i:
            x = local_search_plusplus(data, k_means_plusplus(data, i))
        cluster_centers_list.append(x)

    hierarchical_clustering.append(cs)
    while len(cs) > 1:
        print('Iteration: ' + str(iteration))
        final_return = augment(data, cs, len(cs), iteration, cluster_centers_list, gamma = 2)
        if final_return is not None:
            cs = final_return
            hierarchical_clustering.append(cs)
        iteration += 1

    return hierarchical_clustering, cluster_centers_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--path', default='result.pkl', type=pathlib.Path, help='filepath for saving the result')
    parser.add_argument('--n', default=20, type=int, help='Cardinality of randomly generated set')
    parser.add_argument('--dimension', default=5, type=int, help='dimension of randomly generated set')

    args = parser.parse_args()

    # Initialization
    filepath = args.path
    n = args.n
    coefficient = 1
    S = np.random.random((n, 2))

    # Dilating vectors/points in the euclidean space guaranteeing that no solution below a cost of 1 can be reached
    for k in range(len(S)):
        for l in range(len(S)):
            if not np.array_equal(S[k], S[l]):
                if np.linalg.norm(S[k] - S[l], ord = 2) < 2:

                    coefficient *= (1/np.linalg.norm(S[k] - S[l]))*2
                    S = S*(1/np.linalg.norm(S[k] - S[l]))*2
    S *= 2
    coefficient *= 2

    # Union-HC
    final_clustering, k_m_clustering = algorithm_old(S)
    true_final_clustering = finish_clustering(S, final_clustering)

    test_hierarchical(true_final_clustering)

    # Dump final solution into a pickle file
    with open(filepath, 'wb') as file:
        pickle.dump(true_final_clustering, file)

