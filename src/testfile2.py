import numpy as np
import pickle
import matplotlib.pyplot as plt
import time
import seaborn

np.random.seed(0)

def k_means_plusplus(S, k):

    Z = []
    # Sampling an element with a uniform distribution
    z1 = S[np.random.choice(np.arange(S.shape[0]))]
    Z.append(z1)

    # Initializing distance arrays
    d = np.zeros(S.shape[0])
    for i in range(S.shape[0]):
        d[i] = np.linalg.norm(S[i] - z1, ord = 2)**2

    # Initializing probabilities
    probs = np.zeros(S.shape[0])
    for i in range(1, k):
        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)

        # Sampling next cluster center
        z_new = S[np.random.choice(np.arange(S.shape[0]), p = probs)]
        Z.append(z_new)
        # Calculating minimum distance
        for j in range(S.shape[0]):
            d[j] = np.minimum(d[j], np.linalg.norm(S[j] - z_new, ord=2)**2)
    return np.array(Z)


def calculate_k_means_cluster_cost(data, cluster_centers):
    cluster_sets = {}
    costs = 0
    for i in range(cluster_centers.shape[0]):
        cluster_sets[i] = []
    for x in data:
        cluster_idx = np.argmin([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        costs += np.min([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        cluster_sets[cluster_idx].append(x)

    return cluster_sets, costs

def k_means(S, k, C = None):

    if C is None:
        C = np.random.random((k, S.shape[1]))

    # Error handling
    else:
        assert C.shape[0] == k, "less/more Cluster centers in C than given k"

    # Addition of 1 is needed for while loop to run at least one time
    C_converge = C.copy() + 1

    # Potential floating point error is being considered
    while np.abs(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]) > 0.0001:

        C_converge = C.copy()

        cluster_sets = {}
        for i in range(k):
            cluster_sets[i] = []

        # Computing minimal distance cluster center for each data point
        for x in S:
            cluster_idx = np.argmin([np.linalg.norm(x - c, ord = 2)**2 for c in C])
            cluster_sets[cluster_idx].append(x)

        buffer = 0
        for i in range(k):

            # Error management, in case no data points are in the vicinity of a given cluster center
            if len(cluster_sets[i]) == 0:
                buffer += 1

            else:
                # (i - buffer) is used to facilitate computation of C array with less Clusters
                C[i - buffer] = np.sum(np.array(cluster_sets[i]), axis = 0)/len(cluster_sets[i])

        # If lloyds algorithm algorithm computed less than k Clusters
        if buffer > 0:
            print("algorithm computed less than k clusters, case is being handled")
            k -= buffer
            copy = C.copy()
            C = np.zeros((k, copy.shape[1]))
            for i in range(k):
                C[i] = copy[i]

        if buffer == 0:
            print("following is converge_cost : " + str(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]))

    return cluster_sets, C

S_1 = np.random.random((10, 2)) + 2


S_2 = np.random.random((10, 2)) - 2

S_3 = np.random.random((10, 2))

S = np.vstack((S_1, S_2, S_3))


_, C = k_means(S, 2, C = k_means_plusplus(S, 2))


x_green = [-25, 0, 25]
y_green = [0, 0, 0]
x = [-25, -25, -25, 0, 0, 0, 25, 25, 25]
y = [-1, 0, 1, -1, 0, 1, -1, 0, 1]

x_red = [0, 0, 0]
y_red = [1, 0, -1]

_, axes = plt.subplots(nrows = 2, ncols = 1, figsize=(20, 15))
axes[0].scatter(S_1[:, 0], S_1[:, 1], c = 'black', s=50)
axes[0].scatter(S_2[:, 0], S_2[:, 1], c = 'green', s=50)
axes[0].scatter(S_3[:, 0], S_3[:, 1], c = 'red', s=50)
axes[0].scatter(C[:, 0], C[:, 1], c = 'blue', s=100, marker = 'x')
axes[1].scatter(S_1[:, 0], S_1[:, 1], c = 'black', s=50)
axes[1].scatter(S_2[:, 0], S_2[:, 1], c = 'green', s=50)
axes[1].scatter(S_3[:, 0], S_3[:, 1], c = 'green', s=50)
axes[1].scatter(C[:, 0], C[:, 1], c = 'blue', s=100, marker = 'x')





#plt.scatter(x, y, marker = '.', markersize=20)

plt.show()