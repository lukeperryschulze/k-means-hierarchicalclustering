import numpy as np
import pickle
import matplotlib.pyplot as plt
import time
import seaborn



#filepath = args[0]

#print(filepath)

x_green = [-25, 0, 25]
y_green = [0, 0, 0]
x = [-25, -25, -25, 0, 0, 0, 25, 25, 25]
y = [-1, 0, 1, -1, 0, 1, -1, 0, 1]

x_red = [0, 0, 0]
y_red = [1, 0, -1]

_, axes = plt.subplots(nrows = 2, ncols = 1, figsize=(20, 15))
axes[0].scatter(x, y, c = 'black', s=300)
axes[0].scatter(x_green, y_green, c = 'green', s=300)
axes[1].scatter(x, y, c = 'black', s=300)
axes[1].scatter(x_red, y_red, c = 'red', s=300)

#plt.scatter(x, y, marker = '.', markersize=20)

#plt.show()