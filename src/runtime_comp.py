import numpy as np
import pickle
import matplotlib.pyplot as plt
import time
import pandas as pd
import math

# S = Datenpunktmenge als numpy array, shape = ()
# k = Clusteranzahl

# C in der Regel Clusterzentren (gespeichert als ndarray)
# cluster_sets in der Regel Clustermengen (gespeichert als dictionary)

# fast_k_means_plusplus is a function making use of k_means_plusplus and local_search_plusplus
# the goal is to accelerate distance calculation by saving calculated distanced dynamically and reducing calculation
# quantity, by merging both functions

def k_means_plusplus_new(S, k): # S = Datenpunktmenge, k = Clusteranzahl

    d_point = np.zeros(S.shape[0])
    d = np.zeros(S.shape[0])

    if k == 1:  # needed to reshape C into appropriate shape
        n = np.random.choice(np.arange(S.shape[0]))
        for i in range(S.shape[0]):
            d[i] = np.linalg.norm(S[i] - S[n], ord = 2) ** 2
        d_point[:] = 0
        return S[n].reshape((1, S.shape[1])), d, d_point  # wählt uniform zufälliges Element der Datenpunktmenge


    Z = []
    n = np.random.choice(np.arange(S.shape[0]))

    z1 = S[n]  # wählt uniform zufälliges Element der Datenpunktmenge
    Z.append(z1)

    d = np.zeros(S.shape[0]) # Anzahl der Datenpunkte als Shape
    for i in range(S.shape[0]):
        d[i] = np.linalg.norm(S[i] - z1, ord = 2)**2

    probs = np.zeros(S.shape[0])  # initialize probs variable
    for i in range(1, k):

        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)


        z_new = S[np.random.choice(np.arange(S.shape[0]), p = probs)] # wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung
        Z.append(z_new)
        for i in range(S.shape[0]):
            d_n = np.linalg.norm(S[i] - S[n], ord=2) ** 2
            if d_n < d[i]:
                d[i] = d_n
                d_point[i] = len(Z) - 1



    return np.array(Z), d, d_point


def fast_k_means_plusplus(S, k, d = None, d_point = None, C = None, epsilon = 1):

    if d_point is None:
        d_point = np.zeros(S.shape[0])
    if d is None:
        d = np.zeros(S.shape[0])

    if C is None and k == 1:
        n = np.random.choice(np.arange(S.shape[0]))
        for i in range(S.shape[0]):
            d[i] = np.linalg.norm(S[i] - S[n], ord = 2) ** 2
        d_point[:] = 0
        return S[n].reshape((1, S.shape[1])), d, d_point  # wählt uniform zufälliges Element der Datenpunktmenge


    assert C.shape[0] == k - 1, "eingebenes Cluster hat nicht die korrekte Größe"


    probs = np.zeros(S.shape[0])  # initialize probs variable
    for l in range(S.shape[0]):
        probs[l] = d[l] / np.sum(d)

    n = np.random.choice(np.arange(S.shape[0]),
                               p=probs)
    z_new = S[n].reshape((1, S.shape[1]))  # Wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung

    for i in range(S.shape[0]):
        d_n = np.linalg.norm(S[i] - S[n], ord=2) ** 2
        if d_n < d[i]:
            d[i] = d_n
            d_point[i] = k - 1
    C = np.concatenate((C, z_new), axis = 0)



    # now we do local_search_plusplus iterations
    # if every point is already used as a cluster_center local_search_plusplus is no longer needed



    ##################--------------LOCAL___SEARCH_______++_________##################################################################
    if C.shape[0] == S.shape[0]:
        return C, d, d_point

    for k in range(int(C.shape[0] * epsilon + 1)):

        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)


        n = np.random.choice(np.arange(S.shape[0]),
                                   p=probs)
        z_new = S[n]  # Wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung

        # man berechne hier arg min
        current_cost = np.sum(d)
        C_final = C.copy()
        d_final = d.copy()
        d_point_final = d_point.copy()
        for i in range(C.shape[0]):
            d_copy = d.copy()
            d_point_copy = d_point.copy()
            C_copy = C.copy()
            C_copy[i] = S[n]
            for j in range(S.shape[0]):
                d_new = np.linalg.norm(S[j] - z_new, ord=2) ** 2  # berechnet minimalen Abstand der Datenpunkte zu gewählten Clusterzentren

                if d_new < d_copy[j]:
                    d_copy[j] = d_new
                    d_point_copy[j] = i

                else:
                    if d_point_copy[j] == i:
                        cost_list = [np.linalg.norm(S[j] - c, ord = 2) ** 2 for c in C_copy]
                        new_min_clus_center = np.argmin(cost_list)
                        d_point_copy[j] = new_min_clus_center
                        d_copy[j] = cost_list[new_min_clus_center]

            d_sum = np.sum(d_copy)
            if d_sum < current_cost:
                current_cost = d_sum
                d_final = d_copy
                d_point_final = d_point_copy
                C_final = C_copy
                # print(str(np.count_nonzero(d_copy)) + 'in iteration i: ' + str(i) + ' in iteration j: ' + str(j))

        d = d_final
        d_point = d_point_final
        C = C_final

    return C, d, d_point



def k_means_plusplus(S, k): # S = Datenpunktmenge, k = Clusteranzahl

    Z = []
    z1 = S[np.random.choice(np.arange(S.shape[0]))] # wählt uniform zufälliges Element der Datenpunktmenge
    Z.append(z1)

    d = np.zeros(S.shape[0]) # Anzahl der Datenpunkte als Shape
    for i in range(S.shape[0]):
        d[i] = np.linalg.norm(S[i] - z1, ord = 2)**2

    probs = np.zeros(S.shape[0])  # initialize probs variable
    for i in range(1, k):

        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)


        z_new = S[np.random.choice(np.arange(S.shape[0]), p = probs)] # wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung
        Z.append(z_new)
        for j in range(S.shape[0]):
            d[j] = np.minimum(d[j], np.linalg.norm(S[j] - z_new, ord=2)**2) # berechnet minimalen Abstand der Datenpunkte zu gewählten Clusterzentren
    return np.array(Z)


# algorithm to improve k_means_plus_plus approximation to O(1)
# S: data, C: cluster_centers, epsilon: iterations/k (further explanation can be found in the corresponding paper)
def local_search_plusplus(S, C, epsilon = 1):
    # if every point is already used as a cluster_center local_search_plusplus is no longer needed
    if C.shape[0] == S.shape[0]:
        return C

    probs = np.zeros(S.shape[0])  # initialize probability variable
    d = np.zeros(S.shape[0])

    for k in range(C.shape[0] * epsilon):
        for l in range(S.shape[0]):
            d[l] = np.min([np.linalg.norm(S[l] - c, ord=2) ** 2 for c in C])

        for l in range(S.shape[0]):
            probs[l] = d[l]/np.sum(d)

        z_new = S[np.random.choice(np.arange(S.shape[0]), p=probs)]  # Wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung

        _ , cost = calculate_k_means_cluster_cost(S, C)
        cost_list = []
        final_cost = cost
        final_cluster = C
        for i in range(C.shape[0]):
            cluster_copy = C.copy()
            cluster_copy[i] = z_new
            _ , copy_cost = calculate_k_means_cluster_cost(S, cluster_copy)
            if copy_cost < final_cost:
                final_cost = copy_cost
                final_cluster = cluster_copy
                print("better k_means_plus_plus found")

        C = final_cluster
    return C


def k_means(S, k, C = None, iterations = 1000):

    if C is None:
        C = np.random.random((k, S.shape[1]))

    else:
        assert C.shape[0] == k, "less/more Cluster centers in C than given k"

    C_converge = C.copy() + 1  # +1 needed for while loop to run at least one time

    while np.abs(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]) > 0.0001:

        C_converge = C.copy()

        cluster_sets = {}
        for i in range(k):
            cluster_sets[i] = []

        # computing minimal distance cluster center for each data point
        for x in S:
            cluster_idx = np.argmin([np.linalg.norm(x - c, ord = 2)**2 for c in C])
            cluster_sets[cluster_idx].append(x)

        buffer = 0
        for i in range(k):

            if len(cluster_sets[i]) == 0:  # error management, in case no data points are in the vicinity of a given cluster center
                buffer += 1

            else:
                C[i - buffer] = np.sum(np.array(cluster_sets[i]), axis = 0)/len(cluster_sets[i])  # (i - buffer) to make computation of C array with less Clusters easier

        if buffer > 0:  # if algorithm computed less than k Clusters
            print("algorithm computed less than k clusters, case is being handled")
            k -= buffer
            copy = C.copy()
            C = np.zeros((k, copy.shape[1]))
            for i in range(k):
                C[i] = copy[i]

        if buffer == 0:
            print("following is converge_cost : " + str(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]))

    return cluster_sets, C


# calculates cluster costs if cluster center is understood as the arithmetic median of a given cluster
def calculate_opt_k_means_cost(cluster_set):
    cost = 0
    for i in range(len(cluster_set)):
        median = np.sum(np.array(cluster_set[i]), axis = 0)/len(cluster_set[i])

        new_cost = np.sum([np.linalg.norm(x - median, ord = 2)**2 for x in cluster_set[i]])

        cost+=new_cost

    return cost



# calculates clusters and cluster costs if a set of cluster_centers is given
def calculate_k_means_cluster_cost(data, cluster_centers):
    cluster_sets = {}
    costs = 0
    for i in range(cluster_centers.shape[0]):
        cluster_sets[i] = []
    for x in data:
        cluster_idx = np.argmin([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        costs += np.min([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        cluster_sets[cluster_idx].append(x)

    return cluster_sets, costs

def augment(S, cluster_sets, k, iteration, cluster_centers_list, gamma = 2, use_lloyds = False):

    # lists for final output
    final = []
    final_costs = []

    for p in range(1, k):
        # cluster_centers = k_means_plusplus(S, k - p)
        cluster_centers = cluster_centers_list[k - p - 1]

        if use_lloyds:
            _, cluster_centers = k_means(S, C = cluster_centers, iterations = 100)


        used_cluster_centers = []
        for i in range(k):
            # calculate cluster center with lowest cost to cluster
            used_cluster_centers.append((np.argmin([np.sum([np.linalg.norm(element - c, ord = 2)**2 for element in cluster_sets[i]]) for c in cluster_centers])))  # i think this is correct 06/12/21

        # saving cluster centers, which were 'used' more than once, to facilitate merging step of clusters
        occurrences = [x for x in used_cluster_centers if used_cluster_centers.count(x) > 1]
        k_new = len(occurrences) - 1  # new number of Clusters -> lose one cluster for every single merge step, every element starting from the second one implies a future merge step
        occurrences_no_duplicates = list(set(occurrences))  # deleting duplicates from occurrences for merge step


        # merge step / examining double/multiple occurrences of cluster centers
        new_cluster_sets = {}
        for m, element in enumerate(occurrences_no_duplicates):
            l1 = [l for l, k2 in enumerate(used_cluster_centers) if (k2 == element)]

            new = []
            for l1_element in l1:
                new = new + cluster_sets[l1_element]
            new_cluster_sets[m] = new
            l1 = []

        # examining single occurrences of cluster centers
        # used_cluster_centers was constructed in order -> i'th index element stands for cluster chosen for i'th cluster
        for element in used_cluster_centers:
            if element not in occurrences_no_duplicates:
                m += 1
                new_cluster_sets[m] = cluster_sets[used_cluster_centers.index(element)]

        # new_cluster is finished

        costs = calculate_opt_k_means_cost(new_cluster_sets)
        if costs < (2 * gamma)**(iteration + 1):
            final.append(new_cluster_sets)
            final_costs.append(costs)
        else:
            final.append(new_cluster_sets)
            final_costs.append(float('inf'))


    print("final_costs" + str(final_costs))
    print((2 * gamma)**(iteration + 1))
    idx = np.argmin(final_costs)

    if final_costs[idx] == float('inf'):
        print("all costs +inf")
        return None

    else:
        l2 = []
        for i in range(len(final_costs)):
            if final_costs[i] < float('inf'):
                l2.append(i)

        var = np.max(l2)

        return final[var]


# complete algorithm using all of the defined functions
def algorithm(data):
    old_clust = None
    d = None
    d_point = None
    cs = {}
    for i in range(len(data)):  # cluster_sets in first iteration
        cs[i] = [data[i]]

    hierarchical_clustering = []
    iteration = 0

    # generating k_means_plus_plus solution, for comparison later
    cluster_centers_list = []
    for i in range(1, len(data) + 1):
        print("generating clusters step:" + str(i))

        clust, d, d_point = fast_k_means_plusplus(data, i, d = d, d_point = d_point, C = old_clust)

        old_clust = clust  # saving old clust for fast_k_means_plusplus

        # needed to ensure solution is in O(1) (read paper for further information)
        theoretical, theoretical_d, theoretical_d_point = k_means_plusplus_new(data, i)
        if calculate_k_means_cluster_cost(data, theoretical)[1] < calculate_k_means_cluster_cost(data, clust)[1]:
            print("k_means++ is better than local_search++")
            clust = theoretical
            old_clust = clust
            d = theoretical_d
            d_point = theoretical_d_point
        x = k_means(data, i, C = clust.copy())[1]

        # there is a small probability that k_means returns less than i centers, the following case is being handled here
        for j in range(3):
            if x.shape[0] < i:
                x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)), iterations=10)[1]
            if x.shape[0] == 1:
                break
        # if after 3 iterations we do not have a new solution, we generate a solution without lloyds algorithm, guaranteeing correct cluster size
        if x.shape[0] < i:
            x = local_search_plusplus(data, k_means_plusplus(data, i))
        cluster_centers_list.append(x)

    hierarchical_clustering.append(cs)
    while len(cs) > 1:
        print(iteration)
        final_return = augment(data, cs, len(cs), iteration, cluster_centers_list, gamma = 2)
        if final_return is not None:
            cs = final_return
            hierarchical_clustering.append(cs)
        iteration += 1

    return hierarchical_clustering, cluster_centers_list

    # def augment(S, cluster_sets, k, iteration, cluster_centers_list, gamma = 2, use_lloyds = False):


def finish_clustering(data, h_clustering):
    if len(h_clustering) == len(data):
        return h_clustering


    i = 0
    while len(h_clustering[i]) > 1:
        new_clusters = {}
        merge_list = [[] for element in h_clustering[i + 1]]

        # check if you have >=2 more clusters in next list, than in current one, if you only have one,
        # clustering does not need to be augmented

        if len(h_clustering[i]) - len(h_clustering[i + 1]) > 1:

            # create merge_list
            boolean = True
            for l in range(len(h_clustering[i])):
                for m in range(len(h_clustering[i + 1])):
                    for element in h_clustering[i][l]:  # überprüfe Teilmengen Relation
                        if not any([np.array_equal(element, x) for x in h_clustering[i + 1][m]]):
                            boolean = False

                    if boolean:
                        merge_list[m].append(l)
                    boolean = True

            # merge_list created

            # merging two cluster
            for o in range(len(merge_list)):
                if len(merge_list[o]) > 1:
                    new_clusters[0] = h_clustering[i][merge_list[o][0]] + h_clustering[i][merge_list[o][1]]
                    break

            # adding remaining clusters to clustering
            buffer = 0
            for p in range(len(h_clustering[i])):
                if p == merge_list[o][0] or p == merge_list[o][1]:
                    buffer += 1

                else:
                    new_clusters[p - buffer + 1] = h_clustering[i][p]  # Considering the first slot is already used by our merge step, an addition of 1 to our index is imperative

            h_clustering.insert(i + 1, new_clusters)

        i += 1

    return h_clustering


def test_hierarchical(final_clustering):
    true_final_clustering = final_clustering
    for i in range(len(true_final_clustering) - 1):
        cluster_dict = true_final_clustering[i]
        cluster_dict2 = true_final_clustering[i+1]
        for _, element_list in cluster_dict.items(): # element_list is a cluster, we now want to check if element_list is a subset of another cluster in the next clustering
            are_elements_in_a_element_list_2 = False  # boolean, which saves if a given cluster is a subset of a cluster in a higher layer
            for _, element_list2 in cluster_dict2.items():
                for element in element_list:  # element stands for a element in the cluster
                    if not any([np.array_equal(element, x) for x in element_list2]):  # if no element in the cluster2 is equal to element
                        boolean = False
                    else:
                        boolean = True
                if boolean:
                    are_elements_in_a_element_list_2 = True
            assert are_elements_in_a_element_list_2, "CLUSTERING ERROR" + str(i)

    return True


# complete algorithm using all of the defined functions
def algorithm_old(data):
    cs = {}
    for i in range(len(data)):  # cluster_sets in first iteration
        cs[i] = [data[i]]

    hierarchical_clustering = []
    iteration = 0

    # generating k_means_plus_plus solution, for comparison later
    cluster_centers_list = []
    for i in range(1, len(data) + 1):
        print("generating clusters step:" + str(i))
        x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)), iterations=10)[1]

        # there is a small probability that k_means returns less than i centers, the following case is handled here
        for j in range(3):
            if x.shape[0] < i:
                x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)), iterations=10)[1]
            if x.shape[0] == 1:
                break
        # if after 3 iterations we do not have a new solution, we generate a solution without lloyds algorithm, guaranteeing correct cluster size
        if x.shape[0] < i:
            x = local_search_plusplus(data, k_means_plusplus(data, i))
        cluster_centers_list.append(x)

    hierarchical_clustering.append(cs)
    while len(cs) > 1:
        print(iteration)
        final_return = augment(data, cs, len(cs), iteration, cluster_centers_list, gamma = 2)
        if final_return is not None:
            cs = final_return
            hierarchical_clustering.append(cs)
        iteration += 1

    return hierarchical_clustering, cluster_centers_list


if __name__ == "__main__":

    # Init
    max_n = 51
    faster_algorithm = []
    slower_algorithm = []

    S_list = []

    # run 150 iteration -> faster algorithm
    for n in range(1, max_n):
        coefficient = 1

        coefficient = 1
        S = np.random.random((n, 2))

        S_list.append(S)

        # error check for finish_clustering merge step, maybe add assertion later
        if S.shape != np.unique(S, axis=0).shape:
            print("duplicate in S error!")
            S = np.random.random((n, 2))


        print(S.shape)
        print("TIME!")
        start = time.time()
        # Strecken der Vektoren/Punkte im Raum , um zu garantieren, dass keine Lösung unter Kosten von 1 erreicht werden kann, works so far 07/12/21


        for k in range(len(S)):
            for l in range(len(S)):
                if not np.array_equal(S[k], S[l]):
                    if np.linalg.norm(S[k] - S[l], ord = 2) < 2:

                        coefficient *= (1/np.linalg.norm(S[k] - S[l]))*2
                        S = S*(1/np.linalg.norm(S[k] - S[l]))*2
        S *= 2
        coefficient *= 2
        final_clustering, k_m_clustering = algorithm(S)

        true_final_clustering = finish_clustering(S, final_clustering)
        end = time.time()

        faster_algorithm.append((end - start) / 60)


    # run 150 iteration -> slower algorithm
    for n in range(1, max_n):
        coefficient = 1

        S = S_list[n - 1]


        print(S.shape)
        print("TIME!")
        start = time.time()
        # Strecken der Vektoren/Punkte im Raum , um zu garantieren, dass keine Lösung unter Kosten von 1 erreicht werden kann

        for k in range(len(S)):
            for l in range(len(S)):
                if not np.array_equal(S[k], S[l]):
                    if np.linalg.norm(S[k] - S[l], ord = 2) < 2:

                        coefficient *= (1/np.linalg.norm(S[k] - S[l]))*2
                        S = S*(1/np.linalg.norm(S[k] - S[l]))*2
        S *= 2
        coefficient *= 2
        final_clustering, k_m_clustering = algorithm_old(S)

        true_final_clustering = finish_clustering(S, final_clustering)
        end = time.time()

        slower_algorithm.append((end - start) / 60)




    # Plotting

    x_axis = range(1, max_n)
    plt.xticks(x_axis)
    plt.xlabel('Mächtigkeit')
    plt.ylabel('Laufzeit in Minuten')
    plt.plot(x_axis, faster_algorithm, label='Fast HC - Union')
    plt.plot(x_axis, slower_algorithm, label='Slow HC - Union')
    plt.title('Laufzeitvergleich')
    plt.legend()
    plt.show()

