import numpy as np
import pickle
import matplotlib.pyplot as plt
import time
import pandas as pd
import math

# S = Datenpunktmenge als numpy array, shape = ()
# k = Clusteranzahl

# C in der Regel Clusterzentren (gespeichert als ndarray)
# cluster_sets in der Regel Clustermengen (gespeichert als dictionary)
def fast_k_means_plusplus(S, k, C = None):

    if C is None and k == 1:
        return S[np.random.choice(np.arange(S.shape[0]))].reshape((1, S.shape[1]))  # wählt uniform zufälliges Element der Datenpunktmenge


    assert C.shape[0] == k - 1, "eingebenes Cluster hat nicht die korrekte Größe"
    Z = []


    d = np.zeros(S.shape[0])
    for l in range(S.shape[0]):  # berechnet minimalen Abstand zu Clusterzentren
        d[l] = np.min([np.linalg.norm(S[l] - c, ord=2) ** 2 for c in C])

    probs = np.zeros(S.shape[0])  # initialize probs variable

    for l in range(S.shape[0]):
        probs[l] = d[l] / np.sum(d)

    z_new = S[np.random.choice(np.arange(S.shape[0]),
                               p=probs)].reshape((1, S.shape[1]))  # Wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung

    C = np.concatenate((C, z_new), axis = 0)

    return C

def k_means_plusplus(S, k): # S = Datenpunktmenge, k = Clusteranzahl

    Z = []
    z1 = S[np.random.choice(np.arange(S.shape[0]))] # wählt uniform zufälliges Element der Datenpunktmenge
    Z.append(z1)

    d = np.zeros(S.shape[0]) # Anzahl der Datenpunkte als Shape
    for i in range(S.shape[0]):
        d[i] = np.linalg.norm(S[i] - z1, ord = 2)**2

    probs = np.zeros(S.shape[0])  # initialize probs variable
    for i in range(1, k):

        for l in range(S.shape[0]):
            probs[l] = d[l] / np.sum(d)


        z_new = S[np.random.choice(np.arange(S.shape[0]), p = probs)] # wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung
        Z.append(z_new)
        for j in range(S.shape[0]):
            d[j] = np.minimum(d[j], np.linalg.norm(S[j] - z_new, ord=2)**2) # berechnet minimalen Abstand der Datenpunkte zu gewählten Clusterzentren

    return np.array(Z)


# algorithm to improve k_means_plus_plus approximation to O(1)
# S: data, C: cluster_centers, epsilon: iterations/k (further explanation can be found in the corresponding paper)
def local_search_plusplus(S, C, epsilon = 0.1):
    # if every point is already used as a cluster_center local_search_plusplus is no longer needed
    if C.shape[0] == S.shape[0]:
        return C

    probs = np.zeros(S.shape[0])  # initialize probability variable
    d = np.zeros(S.shape[0])

    for k in range(int(C.shape[0] * epsilon)):
        for l in range(S.shape[0]):
            d[l] = np.min([np.linalg.norm(S[l] - c, ord=2) ** 2 for c in C])

        for l in range(S.shape[0]):
            probs[l] = d[l]/np.sum(d)

        z_new = S[np.random.choice(np.arange(S.shape[0]), p=probs)]  # Wählt zufälliges Element der Datenpunktmenge, gegeben der gerade berechneten Verteilung

        _, cost = calculate_k_means_cluster_cost(S, C)
        cost_list = []
        final_cost = cost
        final_cluster = C
        for i in range(C.shape[0]):  # finding arg min to switch with q
            cluster_copy = C.copy()
            cluster_copy[i] = z_new
            _, copy_cost = calculate_k_means_cluster_cost(S, cluster_copy)
            if copy_cost < final_cost:
                final_cost = copy_cost
                final_cluster = cluster_copy
                # print("better k_means_plus_plus found")

        C = final_cluster
    return C


def k_means(S, k, C = None, iterations = 1000):

    if C is None:
        C = np.random.random((k, S.shape[1]))

    else:
        assert C.shape[0] == k, "less/more Cluster centers in C than given k"

    C_converge = C.copy() + 1

    while np.abs(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]) > 0.0001:

        C_converge = C.copy()

        cluster_sets = {}
        for i in range(k):
            cluster_sets[i] = []

        # computing minimal distance cluster center for each data point
        for x in S:
            cluster_idx = np.argmin([np.linalg.norm(x - c, ord = 2)**2 for c in C])
            cluster_sets[cluster_idx].append(x)

        buffer = 0
        for i in range(k):

            if len(cluster_sets[i]) == 0:  # error management, in case no data points are in the vicinity of a given cluster center
                buffer += 1

            else:
                C[i - buffer] = np.sum(np.array(cluster_sets[i]), axis = 0)/len(cluster_sets[i])  # (i - buffer) to make computation of C array with less Clusters easier

        if buffer > 0:  # if algorithm computed less than k Clusters
            print("algorithm computed less than k clusters, case is being handled")
            k -= buffer
            copy = C.copy()
            C = np.zeros((k, copy.shape[1]))
            for i in range(k):
                C[i] = copy[i]

        if buffer == 0:
            print("following is converge_cost : " + str(calculate_k_means_cluster_cost(S, C_converge)[1] - calculate_k_means_cluster_cost(S, C)[1]))

    return cluster_sets, C


# calculates cluster costs if cluster center is understood as the arithmetic median of a given cluster
def calculate_opt_k_means_cost(cluster_set):
    cost = 0
    for i in range(len(cluster_set)):
        median = np.sum(np.array(cluster_set[i]), axis = 0)/len(cluster_set[i])

        new_cost = np.sum([np.linalg.norm(x - median, ord = 2)**2 for x in cluster_set[i]])
        if new_cost > cost:
            cost = new_cost

    return cost


# calculates clusters and cluster costs if a set of cluster_centers is given
def calculate_k_means_cluster_cost(data, cluster_centers):
    cluster_sets = {}
    costs = 0
    for i in range(cluster_centers.shape[0]):
        cluster_sets[i] = []
    for x in data:
        cluster_idx = np.argmin([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        costs += np.min([np.linalg.norm(x - c, ord=2)**2 for c in cluster_centers])
        cluster_sets[cluster_idx].append(x)

    return cluster_sets, costs

def augment(S, cluster_sets, k, iteration, cluster_centers_list, gamma = 2, use_lloyds = False):

    # lists for final output
    final = []
    final_costs = []

    for p in range(1, k):
        # cluster_centers = k_means_plusplus(S, k - p)
        cluster_centers = cluster_centers_list[k - p - 1]

        if use_lloyds:
            _, cluster_centers = k_means(S, C = cluster_centers, iterations = 100)


        used_cluster_centers = []
        for i in range(k):
            # calculate cluster center with lowest cost to cluster
            used_cluster_centers.append((np.argmin([np.sum([np.linalg.norm(element - c, ord = 2)**2 for element in cluster_sets[i]]) for c in cluster_centers])))  # i think this is correct 06/12/21

        # saving cluster centers, which were 'used' more than once, to facilitate merging step of clusters
        occurrences = [x for x in used_cluster_centers if used_cluster_centers.count(x) > 1]
        k_new = len(occurrences) - 1  # new number of Clusters -> lose one cluster for every single merge step, every element starting from the second one implies a future merge step
        occurrences_no_duplicates = list(set(occurrences))  # deleting duplicates from occurrences for merge step


        # merge step / examining double/multiple occurrences of cluster centers
        new_cluster_sets = {}
        for m, element in enumerate(occurrences_no_duplicates):
            l1 = [l for l, k2 in enumerate(used_cluster_centers) if (k2 == element)]

            new = []
            for l1_element in l1:
                new = new + cluster_sets[l1_element]
            new_cluster_sets[m] = new
            l1 = []

        # examining single occurrences of cluster centers
        # used_cluster_centers was constructed in order -> i'th index element stands for cluster chosen for i'th cluster
        for element in used_cluster_centers:
            if element not in occurrences_no_duplicates:
                m += 1
                new_cluster_sets[m] = cluster_sets[used_cluster_centers.index(element)]

        # new_cluster is finished

        costs = calculate_opt_k_means_cost(new_cluster_sets)
        if costs < (2 * gamma)**(iteration + 1):
            final.append(new_cluster_sets)
            final_costs.append(costs)
        else:
            final.append(new_cluster_sets)
            final_costs.append(float('inf'))


    print("final_costs" + str(final_costs))
    print((2 * gamma)**(iteration + 1))
    idx = np.argmin(final_costs)

    if final_costs[idx] == float('inf'):
        print("all costs +inf")
        return None

    else:
        l2 = []
        for i in range(len(final_costs)):
            if final_costs[i] < float('inf'):
                l2.append(i)

        var = np.max(l2)

        return final[var]


# complete algorithm using all of the defined functions
def algorithm(data):
    old_clust = None
    cs = {}
    for i in range(len(data)):  # cluster_sets in first iteration
        cs[i] = [data[i]]

    hierarchical_clustering = []
    iteration = 0

    # generating k_means_plus_plus solution, for comparison later
    cluster_centers_list = []
    for i in range(1, len(data) + 1):
        print("generating clusters step:" + str(i))

        clust = fast_k_means_plusplus(data, i, C = old_clust)
        clust = local_search_plusplus(data, clust)
        old_clust = clust  # saving old clust for fast_k_means_plusplus

        # needed to ensure solution is in O(1) (read paper for further information)
        theoretical = k_means_plusplus(data, i)
        if calculate_k_means_cluster_cost(data, theoretical)[1] < calculate_k_means_cluster_cost(data, clust)[1]:
            clust = theoretical
        x = k_means(data, i, C= clust)[1]

        # there is a small probability that k_means returns less than i centers, the following case is handled here
        for j in range(3):
            if x.shape[0] < i:
                x = k_means(data, i, C=local_search_plusplus(data, k_means_plusplus(data, i)), iterations=10)[1]
            if x.shape[0] == 1:
                break
        # if after 3 iterations we do not have a new solution, we generate a solution without lloyds algorithm, guaranteeing correct cluster size
        if x.shape[0] < i:
            x = local_search_plusplus(data, k_means_plusplus(data, i))
        cluster_centers_list.append(x)

    hierarchical_clustering.append(cs)
    while len(cs) > 1:
        print(iteration)
        final_return = augment(data, cs, len(cs), iteration, cluster_centers_list, gamma = 2)
        if final_return is not None:
            cs = final_return
            hierarchical_clustering.append(cs)
        iteration += 1

    return hierarchical_clustering, cluster_centers_list

    # def augment(S, cluster_sets, k, iteration, cluster_centers_list, gamma = 2, use_lloyds = False):


def finish_clustering(data, h_clustering):
    if len(h_clustering) == len(data):
        return h_clustering


    i = 0
    while len(h_clustering[i]) > 1:
        new_clusters = {}
        merge_list = [[] for element in h_clustering[i + 1]]

        # check if you have >=2 more clusters in next list, than in current one, if you only have one,
        # clustering does not need to be augmented

        if len(h_clustering[i]) - len(h_clustering[i + 1]) > 1:

            # create merge_list
            boolean = True
            for l in range(len(h_clustering[i])):
                for m in range(len(h_clustering[i + 1])):
                    for element in h_clustering[i][l]:  # überprüfe Teilmengen Relation
                        if not any([np.array_equal(element, x) for x in h_clustering[i + 1][m]]):
                            boolean = False

                    if boolean:
                        merge_list[m].append(l)
                    boolean = True

            # merge_list created

            # merging two cluster
            for o in range(len(merge_list)):
                if len(merge_list[o]) > 1:
                    new_clusters[0] = h_clustering[i][merge_list[o][0]] + h_clustering[i][merge_list[o][1]]
                    break

            # adding remaining clusters to clustering
            buffer = 0
            for p in range(len(h_clustering[i])):
                if p == merge_list[o][0] or p == merge_list[o][1]:
                    buffer += 1

                else:
                    new_clusters[p - buffer + 1] = h_clustering[i][p]  # Considering the first slot is already used by our merge step, an addition of 1 to our index is imperative

            h_clustering.insert(i + 1, new_clusters)

        i += 1

    return h_clustering


def test_hierarchical(final_clustering):
    true_final_clustering = final_clustering
    for i in range(len(true_final_clustering) - 1):
        cluster_dict = true_final_clustering[i]
        cluster_dict2 = true_final_clustering[i+1]
        for _, element_list in cluster_dict.items(): # element_list is a cluster, we now want to check if element_list is a subset of another cluster in the next clustering
            are_elements_in_a_element_list_2 = False  # boolean, which saves if a given cluster is a subset of a cluster in a higher layer
            for _, element_list2 in cluster_dict2.items():
                for element in element_list:  # element stands for a element in the cluster
                    if not any([np.array_equal(element, x) for x in element_list2]):  # if no element in the cluster2 is equal to element
                        boolean = False
                    else:
                        boolean = True
                if boolean:
                    are_elements_in_a_element_list_2 = True
            assert are_elements_in_a_element_list_2, "CLUSTERING ERROR" + str(i)

    return True


def main():
    pass


if __name__ == "__main__":
    coefficient = 1

    # Einlesen der Daten mit pandas
    dftest = pd.read_csv(r'C:\Users\lukes\PycharmProjects\k-means-hierarchicalclustering\testdata\iris.csv', nrows=149)

    # Slicen der Testdaten
    testsl = dftest.values[:, :4]



    S = testsl
    print(S.shape)
    print("TIME!")
    start = time.time()
    # Strecken der Vektoren/Punkte im Raum , um zu garantieren, dass keine Lösung unter Kosten von 1 erreicht werden kann, works so far 07/12/21


    for k in range(len(S)):
        for l in range(len(S)):
            if not np.array_equal(S[k], S[l]):
                if np.linalg.norm(S[k] - S[l], ord = 2) < 2:

                    coefficient *= (1/np.linalg.norm(S[k] - S[l]))*2
                    S = S*(1/np.linalg.norm(S[k] - S[l]))*2
    S *= 2
    coefficient *= 2
    final_clustering, k_m_clustering = algorithm(S)

    true_final_clustering = finish_clustering(S, final_clustering)
    test_hierarchical(true_final_clustering)  # check if clustering is correct/hierarchical


    # following is for comparison and evaluation purposes
    true_final_costs = []
    for element in true_final_clustering:
        true_final_costs.append(calculate_opt_k_means_cost(element)/(coefficient**2)) # calculate hierarchical costs

    true_comp_costs = []
    comp_cluster = []
    comparison = []
    true_comparison = []

    for cluster in k_m_clustering:
        c_sets, k_m_cost = calculate_k_means_cluster_cost(S, cluster)
        comp_cluster.append(c_sets)

    for element in comp_cluster:
        true_comp_costs.append(calculate_opt_k_means_cost(element)/(coefficient**2))

    true_comp_costs.reverse()

    for i, j in zip(true_final_costs, true_comp_costs):
        if j == 0:
            true_comparison.append(1)
        else:
            true_comparison.append(i/j)

    print("Das ist das durschnittliche Verhältnis von hierarchisch und normal")
    print(np.sum(true_comparison)/len(true_comparison))

    '''following is only for plotting'''
    x_axis = range(1, len(true_final_costs) + 1)
    # plt.xticks(x_axis)
    plt.xlabel('Hierarchiestufe')
    plt.ylabel('Kosten')
    plt.plot(x_axis, true_comp_costs, label = 'nicht Hierarchisch')
    plt.plot(x_axis, true_final_costs, label = 'Hierarchisch')
    plt.legend()
    plt.show()

    end = time.time()

    print('time in minutes ' + str((end - start)/60))

    '''
    plt.plot(x_axis, true_comparison)
    plt.xticks(x_axis)
    plt.xlabel('Hierarchiestufe')
    plt.ylabel('Hierarchisch/nicht Hierarchisch')
    plt.show()'''