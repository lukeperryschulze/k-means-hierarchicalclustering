# Implementation of an O(1) - Hierarchical Clustering Algorithm

A detailed and efficient implementation of the framework outlined in the attached paper by Lin et al. can be located in the specified directory. 
The framework was used on k-means hierarchical clustering.
The k-means Augmentation Subroutine used and implemented can be found on page 30 of the dissertation written by Großwendt. 
Please note that the augmentation is actually a (2,4) augmentation, not a (4,2)-augmentation.


# Quick Start


Running HC_Union.py initiates the execution of the efficient implementation of the algorithm on a data set that is uniformly randomly generated. 
The final result is then saved in a pickle file.

When calling HC_Union.py, flags such as -path, -n and -dimension can be added to the command line. 
These flags specify the file path for the final result, the power of the generated data point set, and the fixed dimension of all data points.




Paper: 


IncApprox/Union-HC: https://epubs.siam.org/doi/abs/10.1137/070698257

Dissertation Augment-Union: https://bonndoc.ulb.uni-bonn.de/xmlui/bitstream/handle/20.500.11811/8348/5841.pdf?sequence=1&isAllowed=y